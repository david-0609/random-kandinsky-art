import turtle
import random
import requests
import json
import time
import base64

def get_random_number(min: int, max: int, num=1) -> list | int:

    raw_data = {
        "jsonrpc": "2.0",
        "method": "generateIntegers",
        "params": {
            "apiKey": "e788d99d-fc42-479d-8ac8-975342ed8db5",
            "n": num,
            "min": min,
            "max": max,
            "replacement": True
        },
        'id':1
    }

    headers = {'Content-type': 'application/json','Content-Length': '200', 'Accept': 'application/json'}

    data=json.dumps(raw_data)
    
    use_api = True

    try:
        if use_api:
            print("Sending request")
            response = requests.post(
                url='https://api.random.org/json-rpc/2/invoke',
                data=data,
                headers=headers
                )
            if response.status_code == 200:
                jsn_dict = json.loads(response.text)
                data_list = jsn_dict['result']['random']['data']
                if len(data_list) > 1:
                    return data_list
                else:
                    return data_list[0]
        else:
            raise requests.exceptions.ConnectionError
    except (requests.exceptions.ConnectionError, KeyError):
        print("Cannot connect")
        print("API Query failed, falling back to random library")
        use_api = False
        return_list = []
        for _ in range(num):
            return_list.append(random.randint(min,max))
        if len(return_list) > 1:
            return return_list
        else:
            return return_list[0]

def get_random_rgb() -> tuple:
    numbers = get_random_number(1, 255, 3)
    return numbers[0], numbers[1], numbers[2]

# Set up the turtle
t = turtle.Turtle()
s = turtle.Screen()
s.colormode(255)
t.speed(0)
bg = get_random_number(0,255)
turtle.bgcolor((bg, bg, bg))

# Define a function to draw a circle with a random color
def draw_circle():
    print("drawing circle")
    # Generate a random RGB color
    r, g, b = get_random_rgb()
    t.color(r, g, b)

    # Draw a circle with a random radius
    radius = get_random_number(10,150)
    if get_random_number(1,4) == 4:
        t.begin_fill()
        t.color((get_random_rgb()), (get_random_rgb()))
        t.circle(radius)
        t.end_fill()
    else:
        t.color((get_random_rgb()))
        t.circle(radius)

# Define a function to draw a square with a random color and angle
def draw_polygon(sides: int):
    print("drawing square")
    # Generate a random RGB color
    r, g, b = get_random_rgb()
    t.color(r, g, b)

    # Draw a square with a random side length and angle
    side_length = get_random_number(10,150)
    angle = get_random_number(0,360)
    # angle = ((sides - 2)*180)/sides
    t.right(angle)
    for _ in range(sides+1):
        t.forward(side_length)
        t.right(angle)

# Define a function to draw a line with a random color and length
def draw_line():
    print("drawing line")
    # Generate a random RGB color
    r, g, b = get_random_rgb()
    t.color(r, g, b)

    # Draw a line with a random length and angle
    length = get_random_number(50,450)
    angle = get_random_number(0,360)
    t.right(angle)
    t.forward(length)

def draw_image():
# Draw 100 shapes and lines at random positions and angles on the screen
    strokes = get_random_number(50,150)
    print("Start drawing")
    try:
        for _ in range(strokes):
            x = random.randint(-300, 300)
            y = random.randint(-200, 200)
            t.penup()
            t.goto(x, y)
            t.pendown()

            # Randomly choose a shape or line to draw
            shape_choice = random.random()
            if shape_choice < 0.33:
                draw_circle()
            elif shape_choice < 0.66:
                draw_polygon(get_random_number(3,10))
            else:
                draw_line()
    except KeyboardInterrupt:
        print("Interrupted")
    finally:
        print("End drawing")

        ts = t.getscreen()
        random.seed(time.time())
        filename = random.randint(1,999999999)
        filename_bytes = str(filename).encode("ascii")
        filename_b64 = base64.b64encode(filename_bytes)
        b64_string = filename_b64.decode("ascii") + ".eps"
        ts.getcanvas().postscript(file=b64_string, colormode="color")
        print("Saved")
        turtle.exitonclick()

# Run the program
def main():
    draw_image()
    turtle.exitonclick()

if __name__ == "__main__":
    main()
